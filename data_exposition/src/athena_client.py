from time import sleep

from boto3 import client

NUMBER_OF_RETRY_FOR_ATHENA_QUERY = 500

PAGINATION_LIMIT_ATHENA_RESULT = 1000


def send_query_to_athena(presto_query):
    athena_client = AthenaClient()
    athena_result = athena_client.run_query_faster(presto_query)
    return athena_result


class AthenaClient:
    def __init__(self):
        self.athena = client("athena", region_name="eu-west-3")
        self.database_name = "esme2"
        self.s3_output_location = "s3://s3-job-offer-bucket-17/athena_results"
        self.number_of_retry = NUMBER_OF_RETRY_FOR_ATHENA_QUERY
        self.query_execution_id = None
        self.pagination_limit = PAGINATION_LIMIT_ATHENA_RESULT

    def run_query_faster(self, presto_query):
        self._send_query(presto_query)
        self._wait_for_results()
        return self._get_response_object()

    def _send_query(self, presto_query):
        athena_response = self.athena.start_query_execution(
            QueryString=presto_query,
            QueryExecutionContext={"Database": self.database_name},
            ResultConfiguration={
                "OutputLocation": self.s3_output_location,
                "EncryptionConfiguration": {"EncryptionOption": "SSE_S3"},
            },
            WorkGroup="esme",
        )

        self.query_execution_id = athena_response.get("QueryExecutionId")

    def _wait_for_results(self):
        for i in range(self.number_of_retry):

            query_status = self.athena.get_query_execution(QueryExecutionId=self.query_execution_id)
            query_execution_status = query_status["QueryExecution"]["Status"]["State"]

            if query_execution_status == "SUCCEEDED":
                break
            elif query_execution_status == "FAILED":
                raise Exception("_wait_for_results", "QUERY_EXECUTION_STATUS_IS_FAILED")

            elif query_execution_status == "CANCELLED":
                raise Exception("_wait_for_results", "QUERY_EXECUTION_STATUS_IS_CANCELLED")

            else:
                sleep(0.2)

    def _get_result_iterator(self):
        paginator = self.athena.get_paginator("get_query_results")

        return paginator.paginate(
            QueryExecutionId=self.query_execution_id, PaginationConfig={"PageSize": self.pagination_limit}
        )

    def _get_response_object(self):
        s3 = client("s3")
        response = s3.get_object(
            Bucket=self.s3_output_location.split("/")[2], Key="athena_results/" + self.query_execution_id + ".csv"
        )
        return response
